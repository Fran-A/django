"""
WSGI config for jenkins_test_app project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/wsgi/
"""
# HOLA HUGO x2 231241u4hq0odpfjkqwiudhqwaicjkwoeifhñ oqiujo
import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'jenkins_test_app.settings')

application = get_wsgi_application()
